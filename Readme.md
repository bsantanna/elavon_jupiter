# Jupiter
Projeto Jupiter contém as máquinas virtuais para execução de um ambiente completo de desenvolvimento.
A motivação principal é manter um ambiente único, reprodutível, com capacidade de simulação de erros como indisponibilidade de recursos e principalmente eliminar o problema "funciona na minha máquina", deve funcionar em todas as máquinas!

## Máquinas Virtuais
As máquinas virtuais recebem nomes de alguns dos [satélites naturais de Jupiter](https://en.wikipedia.org/wiki/Galilean_moons).
Para a manutenção do ciclo de vida das máquinas virtuais estamos utilizando o utilitário [Vagrant](https://www.vagrantup.com/), sua implementação foi baseada no livro [Vagrant: Up and Running de Mitchell Hashimoto](http://shop.oreilly.com/product/0636920026358.do).
Para a execução das máquinas virtuais estamos utilizando o utilitário [VirtualBox](http://virtualbox.org/).

### Ciclo de vida das máquinas virtuais
As máquinas virtuais dependem de arquivos chamados ***Vagrantfile*** que estão dispostos em seus respectivos diretórios. O utilitário *vagrant* instala e configura as máquinas virtuais seguindo as configurações especificadas nesses arquivos, tais quais endereço ip, portas, diretórios compartilhados e etc.
Para iniciar uma máquina virtual, entre em seu diretório e digite o seguinte comando:
```
vagrant up
```

Por limitações de licenciamento, as máquinas virtuais que rodam Windows Server 2012 são desligadas automaticamente depois de uma hora de utilização.
Você pode validar se uma máquina virtual está em pé utilizando o comando ping, exemplo:
```
ping 172.16.0.10
```

Ou então utilizando o próprio vagrant:
```
vagrant status
```

Para desligar uma máquina virtual manualmente utilize o seguinte comando:
```
vagrant halt
```

Para mais informações relacionadas a utilização do Vagrant, consulte a [página oficial](https://www.vagrantup.com/).

### Callisto
[Callisto](https://en.wikipedia.org/wiki/Callisto_(moon)) é um [Windows Server 2012](https://en.wikipedia.org/wiki/Windows_Server_2012) Standard convertido em [Server Core](https://en.wikipedia.org/wiki/Windows_Server_2008#Server_Core).
Contém o servidor de aplicação [IIS 8](https://en.wikipedia.org/wiki/Internet_Information_Services) com o ambiente de execução [.NET Framework](https://en.wikipedia.org/wiki/.NET_Framework) 4.5 instalado e configurado.
Esta máquina virtual contém as aplicações do projeto PRJ-WSGATE (ADMArea, ClientWSGate, WSGate, Simulator...), para atualizar a versão das aplicações web dessa máquina virtual existe o script **deploy.bat** que fica na raiz do projeto PRJ-WSGATE.
Para instalar esta máquina virtual no Vagrant utilize o seguinte comando:
```
vagrant box add --name elavon/callisto --provider virtualbox <Caminho>\package.box
```
O IP padrão dessa máquina virtual é 172.16.0.10
Para acessar as aplicações no browser dois endereços podem ser utilizados, exemplos:

* http://172.16.0.10/ADMArea (=> acesso direto, vai funcionar com proxy inativo)
* http://localhost:8080/ADMArea (=> redirecionamento utilizando a máquina local apontando para máquina virtual, vai funcionar com ou sem proxy)

### Io
[Io](https://en.wikipedia.org/wiki/Io_(moon)) é um [Windows Server 2012](https://en.wikipedia.org/wiki/Windows_Server_2012) Standard convertido em [Server Core](https://en.wikipedia.org/wiki/Windows_Server_2008#Server_Core).
Contém o banco de dados da aplicação, um [Microsoft SQL Server 2014](https://en.wikipedia.org/wiki/Microsoft_SQL_Server#SQL_Server_2014).
Para instalar esta máquina virtual no Vagrant utilize o seguinte comando:
```
vagrant box add --name elavon/io --provider virtualbox <Caminho>\package.box
```
O IP padrão dessa máquina virtual é 172.16.0.11

### Ganymede
[Ganymede](https://en.wikipedia.org/wiki/Ganymede_(moon)) é um [Windows Server 2012](https://en.wikipedia.org/wiki/Windows_Server_2012) Standard convertido em [Server Core](https://en.wikipedia.org/wiki/Windows_Server_2008#Server_Core).
[.NET Framework](https://en.wikipedia.org/wiki/.NET_Framework) 4.5 instalado e configurado.
Esta máquina virtual contém os [Serviços do Windows](https://en.wikipedia.org/wiki/Windows_service) que controlam processamentos em lote das transações.
Para instalar esta máquina virtual no Vagrant utilize o seguinte comando:
```
vagrant box add --name elavon/ganymede --provider virtualbox <Caminho>\package.box
```
O IP padrão dessa máquina virtual é 172.16.0.12

## Simulação de Transações (Volume de dados para teste)
Partindo do princípio que as máquinas virtuais **Io** e **Callisto** estejam em pé e respondendo adequadamente é possível simular transações de forma eficiente utilizando o utilitário [Apache JMeter](https://en.wikipedia.org/wiki/Apache_JMeter).
Abaixo um passo a passo com todo o processo para simulação de transações:

### Preparar o banco de dados
- Tecla Window + R => cmd
- No prompt de comando:
```
c:\
cd <Caminho do projeto>\Jupiter\Io
vagrant up
```
- Depois do boot da máquina virtual, abra o [SQL Management Studio](https://en.wikipedia.org/wiki/SQL_Server_Management_Studio) dados de conexão:
  - Host: 172.16.0.11
  - User: wsgateusr
  - Password: usrwsgate
- Execute a [query de limpeza de banco de dados](https://bitbucket.org/bsantanna/elavon_jupiter/src/ef7b74c1a4df52e94b8fa69dba6cb669c35521d4/Io/?at=master) no banco de dados WSGateLocal

### Verificação do ambiente
- Tecla Window + R => cmd
- No prompt de comando:
```
c:\
cd <Caminho do projeto>\Jupiter\Callisto
vagrant up
```
- Depois do boot da máquina virtual, abra o [browser](https://en.wikipedia.org/wiki/Web_browser) e acesse o endereço [http://localhost:8080/ADMArea](http://localhost:8080/ADMArea)
- Login => Transações, deve estar zerado (sem transações)

### Geração de transações
- Caso não tenha instalado na máquina, faça [download do JMeter](https://jmeter.apache.org/download_jmeter.cgi)
- Abra o JMeter, no windows clique 2x em **ApacheJMeter.jar** que está dentro do diretório **bin**
- Menu File => Open
- Localize o arquivo <Caminho do projeto>\Jupiter\Callisto\JMeter\Sale.jmx
- Ajuste os casos de teste, *Terminal 1* e *Terminal 2*, definindo a quantidade de users de cada exemplo, os users no contexto do caso de teste representam o **portador**, por padrão cada terminal tem 20 portadores que fazem transações ao mesmo tempo.
- Inicie o caso de teste apertando o botão play
- Acompanhe o resultado dos testes utilizando o Listener *View Results Tree*
- Espere alguns segundos e interrompa o caso de teste apertando o botão stop
- Acesse o ADMArea e verifique as transações criadas
