# IO

## Queries úteis:

### Limpar banco de dados:
```
truncate table Tasker
print 'tasker'
truncate table LogTasker
print 'LogTasker'
truncate table LogTrans
print 'LogTrans'
truncate table ViaConex
print 'ViaConex'
delete  from  LogCaptureBatchDetail
print 'LogCaptureBatchDetail'
delete  from CaptureBatchDetail
print 'CaptureBatchDetail'
delete  from CaptureBatch
print 'CaptureBatch'
delete  from Transactions
print 'Transactions'
truncate table tasker
print 'tasker'
truncate table LogKeyExchangeTransactions
print 'LogKeyExchangeTransactions'
```

### Adição de um batch com erros na fila de pendentes:
Esta query deve ser utilizada para reprocessamento de um batch quando este for marcado como inválido
```
update capturebatch set statuscode='B0' where BatchID in ('<id_batch_1>', '<id_batch_2>')
update CaptureBatchDetail set statuscode='T0' where BatchID in ('<id_batch_1>', '<id_batch_2>')
update tasker set StatusCodeExpectedForExecution = 'B0' where TransactionID in ('<id_batch_1>', '<id_batch_2>')
```
